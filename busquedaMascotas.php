<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include 'vistas-base/head.php'; ?>
  </head>
  <body onload="imprimirSesion()">
    <?php include 'php/verificarLogin.php' ?>
    <nav>
      <?php include 'vistas-base/nav.php'; ?>
    </nav>


        <aside>
          <?php include 'vistas-base/sidenav.php'; ?>
      </aside>

      <?php  include 'vistas-base/mensaje.php';?>

<main>


      <div class="contenedor-formulario">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <div class="card card-formulario">
                        <div class="card-header">
                            <h4 class="card-title text-center">BUSCAR MASCOTA</h4>
                        </div>
                        <div class="card-body">
                            <form>
                              <div class="row">
                                  <div class="col-md-12">
                                      <div class="form-group">
                                          <label>Ingresa el nombre de la mascota</label>
                                          <div class="row">
                                            <div class="col-md-8">
                                              <input
                                                type="text" class="form-control"
                                                placeholder="Ej. Pulgas" id="mascota">
                                            </div>
                                            <div class="col-md-4">
                                              <button class="btn btn-info btn-fill pull-right" onclick="buscarMascota(event)">Buscar</button>
                                            </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                            </form>
                            <hr>
                            <div id="infoMascotas">

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

      </div>
</main>

  </body>
</html>
