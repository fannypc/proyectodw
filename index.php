<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="resources/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="resources/css/index.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">
    <script src="resources/js/script.js"></script>
    <script src="resources/estilos/js/jquery.js"></script>


  </head>
  <body>

    <div class="mensaje" style="z-index: 9999">
      <h1 id="mensaje"></h1>
    </div>

    <div>
      <img src="resources/images/dog2.jpg" alt="logoPerrito" id="logo-perrito">
    </div>


			<div class="contenedor-login">
				<div onclick="cambio('login','registro')" class="header-login">Iniciar Sesión</div>
				<div onclick="cambio('registro','login')" class="header-login" id="registrarse">Registrarse</div>

			<div class="formulario margin-top" id="login">
				<p>Para continuar, completa tus datos de inicio de sesión.</p>
				<p>Usuario/Correo:</p>
				<input type="text" id="usuario">
				<p>Password:</p>
				<input type="password" id="password">

				<button onclick="login()">Iniciar Sesión</button>

			</div>

			<div class="registro oculto-login margin-top-registro" id="registro">
				<p>Completa los siguientes campos para crear una cuenta en HappyPets.</p>
				<p>Nombre:</p>
				<input type="text" id="nombreDoctor">
				<p>Correo:</p>
				<input type="email" id="correoDoctor">
				<p>Password:</p>
				<input type="password" id="passwordDoctor">
				<p>Repetir password:</p>
				<input type="password" id="confirmaPassword">

				<button onclick="registro(event)">Registrarme</button>
			</div>

			</div>


  </body>
</html>
