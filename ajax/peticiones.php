<?php
// mysqli("set names 'utf8
session_start();

/* Mostrar la codificación de caracteres interna en uso */

$conex = new mysqli('localhost','root','','happypets');
$conex->set_charset("utf8");


if(isset($_GET['m'])){

  $nombreMascota = $_GET['m'];

  $busqueda = 	"SELECT * FROM mascotas WHERE nombre_mascota LIKE '$nombreMascota%'";
  $resultados = $conex->query($busqueda);



  if ($resultados->num_rows > 0) {


    $datosMascotas = array();
      while($row =mysqli_fetch_assoc($resultados))
    {
      array_push($datosMascotas, $row);
    }

    // switch (json_last_error()) {
    //       case JSON_ERROR_NONE:
    //           echo ' - No errors';
    //       break;
    //       case JSON_ERROR_DEPTH:
    //           echo ' - Maximum stack depth exceeded';
    //       break;
    //       case JSON_ERROR_STATE_MISMATCH:
    //           echo ' - Underflow or the modes mismatch';
    //       break;
    //       case JSON_ERROR_CTRL_CHAR:
    //           echo ' - Unexpected control character found';
    //       break;
    //       case JSON_ERROR_SYNTAX:
    //           echo ' - Syntax error, malformed JSON';
    //       break;
    //       case JSON_ERROR_UTF8:
    //           echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
    //       break;
    //       default:
    //           echo ' - Unknown error';
    //       break;
    //   }

    }else{
    	$datosMascotas = array(
    		"status" => "no hay registros"
    	);

    }

    echo json_encode($datosMascotas);

}else if((isset($_GET['idMT']))&&(isset($_GET['t']))&&(isset($_GET['f']))){

  $mascota = $_GET['idMT'];
  $nombreTratamiento = $_GET['t'];
  $fechaTratamiento = $_GET['f'];

  $insertTratamiento="INSERT INTO tratamientos (id_mascota, nombre_tratamiento, fecha_tratamiento)
  VALUES ($mascota, '$nombreTratamiento', '$fechaTratamiento')";

  $resultadoRegistroTratamiento = $conex->query($insertTratamiento);

      if($resultadoRegistroTratamiento){

          $resultadoQueryTratamiento = array(
            "status" => "exito"
          );

      }else{

          $resultadoQueryTratamiento = array(
            "status" => "error"
          );
      }

      echo json_encode($resultadoQueryTratamiento);

}else if((isset($_GET['idMC']))&&(isset($_GET['fc']))&&(isset($_GET['cc']))){

  $mascota = $_GET['idMC'];
  $fechaCita = $_GET['fc'];
  $comentarioCita = $_GET['cc'];

  $insertCita="INSERT INTO citas (id_mascota, fecha_cita, comentario_cita)
  VALUES ($mascota, '$fechaCita', '$comentarioCita')";

  $resultadoRegistroCita = $conex->query($insertCita);

      if($resultadoRegistroCita){

        $resultadoQueryCita = array(
          "status" => "exito"
        );


      }else{

          $resultadoQueryCita = array(
            "status" => "error"
          );
      }

      echo json_encode($resultadoQueryCita);

}else if((isset($_GET['idMCE']))&&(isset($_GET['idce']))&&(isset($_GET['efc']))&&(isset($_GET['ecc']))){

    $idMascota=$_GET['idMCE'];
    $idCita = $_GET['idce'];
    $fechaCita = $_GET['efc'];
    $comentarioCita = $_GET['ecc'];


    $updateCita="UPDATE citas
                  SET fecha_cita='$fechaCita', comentario_cita='$comentarioCita'
                  WHERE id_cita=$idCita";

    $resultadoUpdateCita = $conex->query($updateCita);

        if($resultadoUpdateCita){

          $selectCitas="SELECT *, DATE_FORMAT(fecha_cita, '%Y-%m-%dT%H:%i') AS fecha_cita FROM citas WHERE id_mascota=$idMascota";
          $resultadoCitasM=$conex->query($selectCitas);

          if($resultadoCitasM->num_rows>0){
            $resultadoQueryCita = array();
              while($row =mysqli_fetch_assoc($resultadoCitasM))
            {
              array_push($resultadoQueryCita, $row);
            }
          }

        }else{

            $resultadoQueryCita = array(
              "status" => "error"
            );
        }

        echo json_encode($resultadoQueryCita);

}else if((isset($_GET['idMCELI']))&&(isset($_GET['idceli']))){

      $idMascota=$_GET['idMCELI'];
      $idCita = $_GET['idceli'];


      $deleteCita="DELETE FROM citas
                  WHERE
                  id_cita = $idCita";

      $resultadoDeleteCita = $conex->query($deleteCita);

          $resultadoQueryCita = array();
          if($resultadoDeleteCita){

            $selectCitas="SELECT *, DATE_FORMAT(fecha_cita, '%Y-%m-%dT%H:%i') AS fecha_cita FROM citas WHERE id_mascota=$idMascota";
            $resultadoCitasE=$conex->query($selectCitas);

            if($resultadoCitasE->num_rows>0){
                while($row =mysqli_fetch_assoc($resultadoCitasE))
              {
                array_push($resultadoQueryCita, $row);
              }
            }else{
              $resultadoQueryCita = array(
              );
            }

          }else{

              $resultadoQueryCita = array(
                "status" => "error"
              );
          }

          echo json_encode($resultadoQueryCita);

}else if((isset($_GET['idMIE']))&&(isset($_GET['ndie']))&&(isset($_GET['npie']))
&&(isset($_GET['rie']))&&(isset($_GET['eie']))&&(isset($_GET['cie']))&&(isset($_GET['tie']))
&&(isset($_GET['die']))){


  $idMascota=$_GET['idMIE'];
  $nombreDueno = $_GET['ndie'];
  $nombrePaciente = $_GET['npie'];
  $raza = $_GET['rie'];
  $edad = $_GET['eie'];
  $color = $_GET['cie'];
  $telefono = $_GET['tie'];
  $direccion = $_GET['die'];


  $updateMascota="UPDATE mascotas
                SET nombre_dueno='$nombreDueno', nombre_mascota='$nombrePaciente', raza_mascota='$raza',
                edad_mascota='$edad', color_mascota='$color', telefono_dueno='$telefono',
                direccion_dueno='$direccion'
                WHERE id_mascota=$idMascota";

  $resultadoUpdateMascota = $conex->query($updateMascota);

      if($resultadoUpdateMascota){

        $selectMascotasI="SELECT * FROM mascotas WHERE id_mascota=$idMascota";
        $resultadoMascotasI=$conex->query($selectMascotasI);

        if($resultadoMascotasI->num_rows>0){
          $resultadoQueryMascota = array();
            while($row =mysqli_fetch_assoc($resultadoMascotasI))
          {
            array_push($resultadoQueryMascota, $row);
          }
        }



      }else{

          $resultadoQueryMascota = array(
            "status" => "error"
          );
      }

      echo json_encode($resultadoQueryMascota);


}else if(isset($_GET['idMELI'])){

  $idMascota=$_GET['idMELI'];


  $deleteMascota="DELETE FROM mascotas
              WHERE
              id_mascota = $idMascota";


  $resultadoDeleteMascota = $conex->query($deleteMascota);

  if($resultadoDeleteMascota){

    $resultadoQueryMascota = array(
      "status" => "exito"
    );


  }else{

      $resultadoQueryMascota = array(
        "status" => "error"
      );
  }

  echo json_encode($resultadoQueryMascota);

}else if ((isset($_GET['iddr']))&&(isset($_GET['ndr']))&&(isset($_GET['npr']))&&(isset($_GET['rr']))&&(isset($_GET['er']))&&(isset($_GET['cr']))
  &&(isset($_GET['tr']))&&(isset($_GET['dr']))) {



    $idDoctor= $_GET['iddr'];
    $nombreDueno = $_GET['ndr'];
    $nombrePaciente = $_GET['npr'];
    $raza = $_GET['rr'];
    $edad = $_GET['er'];
    $color = $_GET['cr'];
    $telefono = $_GET['tr'];
    $direccion = $_GET['dr'];

    $insertMascota="INSERT INTO mascotas
                  (id_doctor, nombre_dueno, nombre_mascota, raza_mascota, edad_mascota, color_mascota, telefono_dueno, direccion_dueno)
                  VALUES ($idDoctor, '$nombreDueno', '$nombrePaciente', '$raza', '$edad', '$color', '$telefono', '$direccion')";

      $resultadoInsertMascota = $conex->query($insertMascota);

          if($resultadoInsertMascota){

            $resultadoQueryMascota = array(
              "status" => "exito"
            );

          }else{

              $resultadoQueryMascota = array(
                "status" => "error"
              );
          }

          echo json_encode($resultadoQueryMascota);


}else if((isset($_GET['ndoc']))&&(isset($_GET['cdoc']))&&(isset($_GET['pdoc']))){

  $nombreDoctor= $_GET['ndoc'];
  $correoDoctor = $_GET['cdoc'];
  $passwordDoctor = $_GET['pdoc'];


  $insertDoctor="INSERT INTO doctores
                (nombre_doctor, correo_doctor, password_doctor)
                VALUES ('$nombreDoctor', '$correoDoctor', '$passwordDoctor')";

    $resultadoInsertDoctor = $conex->query($insertDoctor);

        if($resultadoInsertDoctor){

          $_SESSION['login']=true;

          $login = 	"SELECT * FROM doctores
                WHERE correo_doctor = '$correoDoctor' AND password_doctor = '$passwordDoctor'";
          $resultados = $conex->query($login);

        	$res = $resultados->fetch_object();

        	$resultadoQueryInsertD = array(
        		"status" => "exito",
        		"nombre" => $res->nombre_doctor,
        		"id" => $res->id_doctor
        	);

        }else{

            $resultadoQueryInsertD = array(
              "status" => "error"
            );
        }

        echo json_encode($resultadoQueryInsertD);

}else{
  echo "no existe";
}








?>
