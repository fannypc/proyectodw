<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include 'vistas-base/head.php';?>
    <script src="resources/js/traerMascotas.js"></script>

  </head>
  <body onload="imprimirSesion()">

        <nav>
      		<?php include 'vistas-base/nav.php';?>
      	</nav>

        <aside>
          <?php include 'vistas-base/sidenav.php';?>
      </aside>

        <?php include 'vistas-base/mensaje.php';?>
<main>


      <div class="contenedor-formulario">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title text-center">REGISTRAR TRATAMIENTO</h4>
                        </div>
                        <div class="card-body">
                            <form id="formTratamiento">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>1. Ingresar paciente</label>
                                              <select class="form-control" id="selectMascotas">
                                              </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label>2. Tratamiento</label><br>
                                      <input
                                        type="text" class="form-control"
                                        placeholder="Ej. Vacuna contra la rabia" id="nombreTratamiento">
                                    </div>
                                  </div>
                                </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label>3. Fecha del tratamiento</label><br>
                                     <input
                                      type="datetime-local" class="form-control"
                                      placeholder="Ej. Pastor Alemán" id="fechaTratamiento">
                                  </div>
                                </div>
                              </div>

                                <button class="btn btn-info btn-fill pull-right" onclick="registrarTratamiento(event)">Registrar tratamiento</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

      </div>
</main>

  </body>
</html>
