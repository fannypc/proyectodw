<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include 'vistas-base/head.php';?>
  </head>
  <body onload="imprimirSesion()">

        <nav>
          <?php include 'vistas-base/nav.php';?>
      	</nav>

        <aside>
          <?php include 'vistas-base/sidenav.php';?>
      </aside>

      <?php include 'vistas-base/mensaje.php';?>

<main>


      <div class="contenedor-formulario">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title text-center">REGISTRAR NUEVO PACIENTE</h4>
                        </div>
                        <div class="card-body">
                            <form id="registrarMForm">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>1. Nombre del paciente</label>
                                            <input
                                              type="text" class="form-control"
                                              placeholder="Ej. Pulgas" id="nombrePaciente">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label>2. Nombre del dueño</label><br>
                                        <input
                                          type="text" class="form-control"
                                          placeholder="Ej. Ana Mendoza" id="nombreDueno">
                                      </div>
                                    </div>
                                </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>3. Raza</label><br>
                                     <input
                                      type="text" class="form-control"
                                      placeholder="Ej. Pastor Alemán" id="raza">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>4. Edad</label><input type="text"
                                      class="form-control" placeholder="Ej. 4 años" id="edad">
                                  </div>
                                </div>
                              </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>5. Color</label><input type="text"
                                    class="form-control" placeholder="Ej. Café claro" id="color">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>6. Teléfono de contacto</label><br>
                                  <input type="text"
                                    class="form-control" placeholder="Ej. 4421234567" id="telefono">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>7. Dirección</label><br>
                                  <input type="text" class="form-control"
                                    placeholder="Ej. Av Álamo 4 int 42 Col. Paseos del Bosque" id="direccion">
                                </div>
                              </div>
                            </div>
                                <button class="btn btn-info btn-fill pull-right" onclick="registrarMascota(event)">Registrar paciente</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

      </div>
</main>

  </body>
</html>
