<?php
  include 'php/infoMascotas.php';
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include 'vistas-base/head.php';?>
  </head>
  <body onload="imprimirSesion()">

        <nav>
          <?php include 'vistas-base/nav.php';?>
      	</nav>

        <aside>
          <?php include 'vistas-base/sidenav.php';?>
      </aside>

      <?php include 'vistas-base/mensaje.php';?>

<main>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <div class="card card-formulario">
                        <div class="card-header">
                            <h4 class="card-title text-center">EDITAR PACIENTE</h4>
                        </div>
                        <div class="card-body">
                          <div id="infoDespliegue">
                            <?php foreach($arrayInfo as $row){ ?>
                                <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label>Nombre del dueño: </label>
                                          <label><?php echo $row['nombre_dueno'];?></label>
                                      </div>
                                  </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nombre del paciente: </label>
                                            <label><?php echo $row['nombre_mascota'];?></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label>Raza: </label>
                                          <label><?php echo $row['raza_mascota'];?></label>
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label>Edad: </label>
                                          <label><?php echo $row['edad_mascota'];?></label>
                                      </div>
                                  </div>
                                </div>
                              <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Color: </label>
                                        <label><?php echo $row['color_mascota'];?></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Teléfono de contacto: </label>
                                        <label><?php echo $row['telefono_dueno'];?></label>
                                    </div>
                                </div>
                              </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>Dirección:</label><br>
                                  <label><?php echo $row['direccion_dueno'];?></label>
                                </div>
                              </div>
                            </div>

                            <button type="button" class="btn btn-info btn-fill pull-right" data-toggle="modal" data-target="#infoModal<?php echo $row['id_mascota'];?>">
                              Editar
                            </button>
                            <button type="submit" class="btn btn-info btn-fill pull-right" onclick="eliminarMascota(event, <?php echo $row['id_mascota'];?>)">Eliminar</button>
                            <div class="clearfix"></div>
                            <hr style="margin-top:5px;margin-bottom:5px;"><br>

                            <!-- :::::::::::::::::::::::::::::::::::::::::::: MODAL EDITAR INFO ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
                            <div class="modal fade" id="infoModal<?php echo $row['id_mascota'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Información de la mascota</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form id="editarInfoMascota">
                                  <div class="modal-body">
                                        <div class="row">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>Nombre del dueño: </label>
                                                  <input type="text" name="" value="<?php echo $row['nombre_dueno'];?>"
                                                  id="nombreDueno<?php echo $row['id_mascota'];?>">
                                              </div>
                                          </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Nombre del paciente: </label>
                                                    <input type="text" name="" value="<?php echo $row['nombre_mascota'];?>"
                                                    id="nombrePaciente<?php echo $row['id_mascota'];?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>Raza: </label>
                                                  <input type="text" name="" value="<?php echo $row['raza_mascota'];?>"
                                                  id="raza<?php echo $row['id_mascota'];?>">
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>Edad: </label>
                                                  <input type="text" name="" value="<?php echo $row['edad_mascota'];?>"
                                                  id="edad<?php echo $row['id_mascota'];?>">
                                              </div>
                                          </div>
                                        </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Color: </label>
                                                <input type="text" name="" value="<?php echo $row['color_mascota'];?>"
                                                id="color<?php echo $row['id_mascota'];?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Teléfono de contacto: </label>
                                                <input type="text" name="" value="<?php echo $row['telefono_dueno'];?>"
                                                id="telefono<?php echo $row['id_mascota'];?>">
                                            </div>
                                        </div>
                                      </div>
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label>Dirección:</label><br>
                                          <input type="text" name="" value="<?php echo $row['direccion_dueno'];?>"
                                          id="direccion<?php echo $row['id_mascota'];?>">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary" onclick="editarInfoMascota(event, <?php echo $row['id_mascota'];?>)">Guardar</button>
                                    <button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                  </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        <?php } ?>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <div class="accordion" id="accordionExample">
                                    <div class="card">
                                      <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                          <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Ver tratamientos
                                          </button>
                                        </h5>
                                      </div>

                                      <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">

                                          <?php foreach($arrayTratamientos as $tratamiento){ ?>
                                          <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                <label>Tratamiento:</label><br>
                                                <label><?php echo $tratamiento['nombre_tratamiento'];?></label>
                                              </div>
                                            </div>
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                <label>Fecha:</label><br>
                                                <label><?php echo $tratamiento['fecha_tratamiento'];?></label>
                                              </div>
                                            </div>
                                          </div>
                                          <hr style="margin-top:10px;">
                                        <?php } ?>
                                        </div>
                                      </div>
                                    </div>

                                      <!-- ================ MODAL CITAS ==============  -->

                                    <div class="card">
                                      <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                          <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Ver citas
                                          </button>
                                        </h5>
                                      </div>
                                      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        <div class="card-body" id="citasDespliegue">
                                          <?php foreach($arrayCitas as $cita){ ?>
                                          <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                <label>Fecha:</label><br>
                                                <label><?php echo $cita['fecha_cita'];?></label>
                                              </div>
                                            </div>
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                <label>Comentario:</label><br>
                                                <label><?php echo $cita['comentario_cita'];?></label>
                                              </div>
                                            </div>
                                          </div>
                                          <button type="button" class="btn btn-info btn-fill pull-right" data-toggle="modal"
                                          data-target="#citaModal<?php echo $cita['id_cita'];?>">
                                            Editar
                                          </button>

                                          <button type="submit" class="btn btn-info btn-fill pull-right" onclick="eliminarCita(event,
                                          <?php echo $cita['id_cita'];?>, <?php echo $cita['id_mascota'];?>)">Eliminar</button>
                                          <div class="clearfix"></div>
                                          <hr style="margin-top:10px;">

                                          <!-- =================== MODAL CITAS ======================================== -->

                                          <div class="modal fade" id="citaModal<?php echo $cita['id_cita'];?>" tabindex="-1" role="dialog"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <h5 class="modal-title" id="exampleModalLabel">Cita</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <form id="editarInfoMascota">
                                                <div class="modal-body">

                                                  <div class="row">
                                                    <div class="col-md-12">
                                                      <div class="form-group">
                                                        <label>Fecha:</label><br>
                                                        <input type="datetime-local" name="" value="<?php echo $cita['fecha_cita'];?>"
                                                        id="fechaCita<?php echo $cita['id_cita'];?>">
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <div class="row">
                                                    <div class="col-md-12">
                                                      <div class="form-group">
                                                        <label>Comentario:</label><br>
                                                        <input type="text" name="" value="<?php echo $cita['comentario_cita'];?>"
                                                        id="comentarioCita<?php echo $cita['id_cita'];?>">
                                                      </div>
                                                    </div>
                                                  </div>

                                                </div>
                                                <div class="modal-footer">
                                                  <button class="btn btn-primary" onclick="editarCitaMascota(event, <?php echo $cita['id_cita'];?>,
                                                    <?php echo $cita['id_mascota'];?>)">Guardar</button>
                                                  <button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                </div>
                                                </form>
                                              </div>
                                            </div>
                                          </div>


                                        <?php } ?>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




</main>

  </body>
</html>
