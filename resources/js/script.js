function cambio(actual, anterior){
	document.querySelector('#' + anterior).classList.add('oculto-login')
	setTimeout(function(){
		document.querySelector('#' + actual).classList.remove('oculto-login')
	},400)
}

function mensaje(msj){
	document.querySelector('.mensaje').style.transition=".2s all";
	document.querySelector('.mensaje').style.top="0";
	document.querySelector('#mensaje').innerHTML = msj;
	setTimeout(function(){
		document.querySelector('.mensaje').style.top="-40px";
	},3000)
}

function login(){

	usuario = document.getElementById('usuario').value;
	password = document.getElementById('password').value;
	//VALIDAR CAMPOS COMPLETOS
	if (usuario != "" && password != "") {
		loginAJAX = new XMLHttpRequest();
		loginAJAX.open('GET','php/login.php?u='+usuario+'&p='+password);
		loginAJAX.send();
		loginAJAX.onreadystatechange = function(){
			if (loginAJAX.readyState == 4 && loginAJAX.status == 200) {
				JSONrespuesta = JSON.parse(loginAJAX.responseText);
				if (JSONrespuesta.status == "correcto") {
					localStorage.setItem('id', JSONrespuesta.id)
					localStorage.setItem('nombre', JSONrespuesta.nombre)
					window.location.assign('dashboard.php');
				}else{

					window.scrollTo({
						top:0,
						behavior: "smooth"
					});

					setTimeout(function () {
						mensaje('Los datos son incorrectos');
					}, 100);
				}
			}
		}
	}else{
		window.scrollTo({
			top:0,
			behavior: "smooth"
		});

		setTimeout(function () {
			mensaje('Por favor completa los campos, ninguno puede ir vacío :)');
		}, 100);
	}
}

function imprimirSesion(){
	var nombreSesion = localStorage.getItem('nombre');
	document.getElementById("nombreSesion").innerHTML=nombreSesion;
	// console.log(storedName);
}

function registro(e){
	e.preventDefault();

		var nombreDoctor=document.getElementById('nombreDoctor').value;
		var correoDoctor=document.getElementById('correoDoctor').value;
		var passwordDoctor=document.getElementById('passwordDoctor').value;
		var confirmaPassword=document.getElementById('confirmaPassword').value;

		if (nombreDoctor != "" && correoDoctor!=""&& passwordDoctor!=""&&confirmaPassword!=""){

			if(passwordDoctor==confirmaPassword){

				registroAJAX = new XMLHttpRequest();
				registroAJAX.open('GET','ajax/peticiones.php?ndoc='+nombreDoctor+'&cdoc='+correoDoctor+'&pdoc='+passwordDoctor);
				registroAJAX.send();
				registroAJAX.onreadystatechange = function(){
					if (registroAJAX.readyState == 4 && registroAJAX.status == 200) {
						JSONrespuesta = JSON.parse(registroAJAX.responseText);

						if(JSONrespuesta.status=="error"){
							mensaje('Hubo un error en el registro. Intentelo nuevamente');

						}else if(JSONrespuesta.status=="exito"){

									localStorage.setItem('id', JSONrespuesta.id);
									localStorage.setItem('nombre', JSONrespuesta.nombre);
									window.location.href = "dashboard.php";

											}
										}
						}

			}else{
				window.scrollTo({
					top:0,
					behavior: "smooth"
				});

				setTimeout(function () {
					mensaje('La contraseña y su confirmación no son iguales');
				}, 100);

			}

		}else{
			window.scrollTo({
				top:0,
				behavior: "smooth"
			});

			setTimeout(function () {
				mensaje('Por favor completa los campos, ninguno puede ir vacío :)');
			}, 100);
		}
}

function logout(){

	logoutAJAX = new XMLHttpRequest();
	logoutAJAX.open('GET','php/logout.php');
	logoutAJAX.send();
	logoutAJAX.onreadystatechange = function(){
		localStorage.clear();
		window.location.href = "index.php";
	}

}

function registrarMascota(e){
	e.preventDefault();

	var idForm = document.querySelector("form").id;
	var idDoctor = localStorage.getItem('id');
	var nombrePaciente=document.getElementById('nombrePaciente').value;
	var nombreDueno=document.getElementById('nombreDueno').value;
	var raza=document.getElementById('raza').value;
	var edad=document.getElementById('edad').value;
	var color=document.getElementById('color').value;
	var telefono=document.getElementById('telefono').value;
	var direccion=document.getElementById('direccion').value;

	if (nombreDueno != "" && nombrePaciente!=""&& raza!=""&& edad!=""&& color!=""
	&& telefono!=""&& direccion!=""){

		registroAJAX = new XMLHttpRequest();
		registroAJAX.open('GET','ajax/peticiones.php?iddr='+idDoctor+'&ndr='+nombreDueno+'&npr='+nombrePaciente+'&rr='+raza
		+'&er='+edad+'&cr='+color+'&tr='+telefono+'&dr='+direccion);
		registroAJAX.send();
		registroAJAX.onreadystatechange = function(){
			if (registroAJAX.readyState == 4 && registroAJAX.status == 200) {
				registroMascotas = JSON.parse(registroAJAX.responseText);

				if(registroMascotas.status=="error"){
					setTimeout(function () {
						window.scrollTo({
							top:0,
							behavior: "smooth"
						});
					}, 100);

					mensaje('No se pudo registrar la mascota. Intentelo nuevamente');
				}else if(registroMascotas.status=="exito"){
					setTimeout(function () {
						window.scrollTo({
							top:0,
							behavior: "smooth"
						});
					}, 800);

						mensaje('La mascota se registró con exito :)')
						limpiarInputsForm(idForm);
									}
								}
				}

	}else{
		mensaje('Por favor verifica los campos, ninguno puede ir vacío :)');
	}


}

function buscarMascota(e){
	e.preventDefault();

		mascota = document.getElementById('mascota').value;
		var txt = "";
		//VALIDAR CAMPOS COMPLETOS
		if (mascota != "") {
			loginAJAX = new XMLHttpRequest();
			loginAJAX.open('GET','ajax/peticiones.php?m='+mascota);
			loginAJAX.send();
			loginAJAX.onreadystatechange = function(){
				if (loginAJAX.readyState == 4 && loginAJAX.status == 200) {

					// console.log(loginAJAX.responseText);
					mascotas = JSON.parse(loginAJAX.responseText);

					if(mascotas.status=="no hay registros"){
						mensaje('No existen registros de mascotas con ese nombre :(');
					}else{


												txt += ''
											         for (mascota in mascotas) {
											             txt += '<form><div class="row"><div class="col-md-12"><div class="form-group"><label>INFORMACIÓN</label><br><br>'+
																	 '<label>Nombre:&nbsp</label><label>'
																	 + mascotas[mascota].nombre_mascota+'</label><br><label>Nombre del dueño:&nbsp</label><label>'+
																	 mascotas[mascota].nombre_dueno+'</label>';

																	 txt+='</div><a href="editarMascota.php?idM='+mascotas[mascota].id_mascota+'" class="btn btn-info btn-fill pull-right">Detalles</a></div></div></form><hr>'
											         }
											  txt += ''
											         document.getElementById("infoMascotas").innerHTML = txt;


												// window.location.assign('busquedaMascotas.html');
											// }else{
											// 	mensaje('ERROR!, los datos son incorrectos');
											// }
										}
									}

					}

		}else{
			mensaje('Por favor, ingresa el nombre de una mascota :)');
		}

}


function editarInfoMascota(e, idMascota){
	e.preventDefault();
	var modal =document.getElementById("infoModal"+idMascota);
	var idMascota=idMascota;
	var nombreDueno = document.getElementById('nombreDueno'+idMascota).value;
	var nombrePaciente = document.getElementById('nombrePaciente'+idMascota).value;
	var raza = document.getElementById('raza'+idMascota).value;
	var edad = document.getElementById('edad'+idMascota).value;
	var color = document.getElementById('color'+idMascota).value;
	var telefono = document.getElementById('telefono'+idMascota).value;
	var direccion = document.getElementById('direccion'+idMascota).value;

	var txt="";
if (nombreDueno != "" && nombrePaciente!=""&& raza!=""&& edad!=""&& color!=""
&& telefono!=""&& direccion!="") {

	loginAJAX = new XMLHttpRequest();
	loginAJAX.open('GET','ajax/peticiones.php?idMIE='+idMascota+'&ndie='+nombreDueno+'&npie='+nombrePaciente+
	'&rie='+raza+'&eie='+edad+'&cie='+color+'&tie='+telefono+'&die='+direccion);
	loginAJAX.send();
	loginAJAX.onreadystatechange = function(){
		if (loginAJAX.readyState == 4 && loginAJAX.status == 200) {

			infoMascotaEditadas = JSON.parse(loginAJAX.responseText);

			if(infoMascotaEditadas.status=="error"){
							$(modal).modal('hide');

							setTimeout(function () {
								window.scrollTo({
									top:0,
									behavior: "smooth"
								});
							}, 800);

					  	mensaje('No se pudo editar la mascota. Intente de nuevo');
			}else{

				$(modal).modal('hide');

				setTimeout(function () {
					window.scrollTo({
						top:0,
						behavior: "smooth"
					});
				}, 800);

				mensaje ("Se editó la mascota exitosamente");

					for (info in infoMascotaEditadas){
				              						txt+='<div class="row">'+
				                                  '<div class="col-md-6">'+
				                                      '<div class="form-group">'+
				                                          '<label>Nombre del dueño:&nbsp</label>'+
				                                          '<label>'+infoMascotaEditadas[info].nombre_dueno+'</label>'+
				                                      '</div>'+
				                                  '</div>'+
				                                    '<div class="col-md-6">'+
				                                        '<div class="form-group">'+
				                                            '<label>Nombre del paciente:&nbsp</label>'+
				                                            '<label>'+infoMascotaEditadas[info].nombre_mascota+'</label>'+
				                                        '</div>'+
				                                    '</div>'+
				                                '</div>'+
				                                '<div class="row">'+
				                                  '<div class="col-md-6">'+
				                                      '<div class="form-group">'+
				                                          '<label>Raza:&nbsp</label>'+
				                                          '<label>'+infoMascotaEditadas[info].raza_mascota+'</label>'+
				                                      '</div>'+
				                                  '</div>'+
				                                  '<div class="col-md-6">'+
				                                      '<div class="form-group">'+
				                                          '<label>Edad:&nbsp</label>'+
				                                          '<label>'+infoMascotaEditadas[info].edad_mascota+'</label>'+
				                                      '</div>'+
				                                  '</div>'+
				                                '</div>'+
				                              '<div class="row">'+
				                                '<div class="col-md-6">'+
				                                    '<div class="form-group">'+
				                                        '<label>Color:&nbsp</label>'+
				                                        '<label>'+infoMascotaEditadas[info].color_mascota+'</label>'+
				                                    '</div>'+
				                                '</div>'+
				                                '<div class="col-md-6">'+
				                                    '<div class="form-group">'+
				                                        '<label>Teléfono de contacto:&nbsp</label>'+
				                                        '<label>'+infoMascotaEditadas[info].telefono_dueno+'</label>'+
				                                    '</div>'+
				                                '</div>'+
				                              '</div>'+
				                            '<div class="row">'+
				                              '<div class="col-md-12">'+
				                                '<div class="form-group">'+
				                                  '<label>Dirección:&nbsp</label><br>'+
				                                  '<label>'+infoMascotaEditadas[info].direccion_dueno+'</label>'+
				                                '</div>'+
				                              '</div>'+
				                            '</div>'+

				                            '<button type="button" class="btn btn-info btn-fill pull-right" data-toggle="modal" data-target="#infoModal'+infoMascotaEditadas[info].id_mascota+'">'+
				                              'Editar'+
				                            '</button>'+
				                            '<button type="submit" class="btn btn-info btn-fill pull-right" onclick="eliminarMascota(event, '+infoMascotaEditadas[info].id_mascota+')">Eliminar</button>'+
				                            '<div class="clearfix"></div>'+
				                            '<hr style="margin-top:5px;margin-bottom:5px;"><br>'+
					'<div class="modal fade" id="infoModal'+infoMascotaEditadas[info].id_mascota+'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
				  '<div class="modal-dialog" role="document">'+
				    '<div class="modal-content">'+
				      '<div class="modal-header">'+
				        '<h5 class="modal-title" id="exampleModalLabel">Información de la mascota</h5>'+
				        '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
				          '<span aria-hidden="true">&times;</span>'+
				        '</button>'+
				      '</div>'+
				      '<form id="editarInfoMascota">'+
				      '<div class="modal-body">'+
				            '<div class="row">'+
				              '<div class="col-md-6">'+
				                  '<div class="form-group">'+
				                      '<label>Nombre del dueño: </label>'+
				                      '<input type="text" name="" value="'+infoMascotaEditadas[info].nombre_dueno+'"'+
				                      'id="nombreDueno'+infoMascotaEditadas[info].id_mascota+'">'+
				                  '</div>'+
				              '</div>'+
				                '<div class="col-md-6">'+
				                    '<div class="form-group">'+
				                        '<label>Nombre del paciente: </label>'+
				                        '<input type="text" name="" value="'+infoMascotaEditadas[info].nombre_mascota+'"'+
				                        'id="nombrePaciente'+infoMascotaEditadas[info].id_mascota+'">'+
				                    '</div>'+
				                '</div>'+
				            '</div>'+
				            '<div class="row">'+
				              '<div class="col-md-6">'+
				                  '<div class="form-group">'+
				                      '<label>Raza: </label>'+
				                      '<input type="text" name="" value="'+infoMascotaEditadas[info].raza_mascota+'"'+
				                      'id="raza'+infoMascotaEditadas[info].id_mascota+'">'+
				                  '</div>'+
				              '</div>'+
				              '<div class="col-md-6">'+
				                  '<div class="form-group">'+
				                      '<label>Edad: </label>'+
				                      '<input type="text" name="" value="'+infoMascotaEditadas[info].edad_mascota+'"'+
				                      'id="edad'+infoMascotaEditadas[info].id_mascota+'">'+
				                  '</div>'+
				              '</div>'+
				            '</div>'+
				          '<div class="row">'+
				            '<div class="col-md-6">'+
				                '<div class="form-group">'+
				                    '<label>Color: </label>'+
				                    '<input type="text" name="" value="'+infoMascotaEditadas[info].color_mascota+'"'+
				                    'id="color'+infoMascotaEditadas[info].id_mascota+'">'+
				                '</div>'+
				            '</div>'+
				            '<div class="col-md-6">'+
				                '<div class="form-group">'+
				                    '<label>Teléfono de contacto: </label>'+
				                    '<input type="text" name="" value="'+infoMascotaEditadas[info].telefono_dueno+'"'+
				                    'id="telefono'+infoMascotaEditadas[info].id_mascota+'">'+
				                '</div>'+
				            '</div>'+
				          '</div>'+
				        '<div class="row">'+
				          '<div class="col-md-12">'+
				            '<div class="form-group">'+
				              '<label>Dirección:</label><br>'+
				              '<input type="text" name="" value="'+infoMascotaEditadas[info].direccion_dueno+'"'+
				              'id="direccion'+infoMascotaEditadas[info].id_mascota+'">'+
				            '</div>'+
				          '</div>'+
				        '</div>'+
				      '</div>'+
				      '<div class="modal-footer">'+
				        '<button type="submit" class="btn btn-primary" onclick="editarInfoMascota(event, '+infoMascotaEditadas[info].id_mascota+')">Guardar</button>'+
				        '<button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>'+
				      '</div>'+
				      '</form>'+
				    '</div>'+
				  '</div>'+
				'</div>';
}
				document.getElementById('infoDespliegue').innerHTML=txt;

							}
			}
	}
}else{

	setTimeout(function () {
		window.scrollTo({
			top:0,
			behavior: "smooth"
		});
	}, 800);

	mensaje('Por favor verifica los campos, ninguno puede ir vacío :)');
}

}


function editarCitaMascota(e, idCita, idMascota){
	e.preventDefault();

	var modal =document.getElementById("citaModal"+idCita);
	var idMascota=idMascota;
	var idCita=idCita;
	var fechaCita = document.getElementById('fechaCita'+idCita).value;
	var comentarioCita=document.getElementById('comentarioCita'+idCita).value;

		var txt="";
	if (fechaCita != "") {

		loginAJAX = new XMLHttpRequest();
		loginAJAX.open('GET','ajax/peticiones.php?idMCE='+idMascota+'&idce='+idCita+'&efc='+fechaCita+'&ecc='+comentarioCita);
		loginAJAX.send();
		loginAJAX.onreadystatechange = function(){
			if (loginAJAX.readyState == 4 && loginAJAX.status == 200) {
				// console.log(loginAJAX.responseText);
				citas = JSON.parse(loginAJAX.responseText);

				if(citas.status=="error"){

					$(modal).modal('hide');

					setTimeout(function () {
						window.scrollTo({
							top:0,
							behavior: "smooth"
						});
					}, 800);

										mensaje('No se pudo editar la cita. Intente de nuevo');
				}else{

					$(modal).modal('hide');

					setTimeout(function () {
						window.scrollTo({
							top:0,
							behavior: "smooth"
						});
					}, 800);

					mensaje ("Se editó exitosamente la cita en la base de datos");

					for (cita in citas){
						txt+='<div class="row">'+
						'<div class="col-md-6">'+
						'<div class="form-group">'+
								'<label>Fecha:</label><br>'+
								'<label>'+citas[cita].fecha_cita+'</label>'+
							'</div>'+
						'</div>'+
						'<div class="col-md-6">'+
							'<div class="form-group">'+
								'<label>Comentario:</label><br><label>'+
								citas[cita].comentario_cita+
								'</label>'+
							'</div>'+
						'</div>'+
					'</div>'+
					'<button type="button" class="btn btn-info btn-fill pull-right" data-toggle="modal" data-target="#citaModal'+citas[cita].id_cita+'">'+
						'Editar'+
					'</button>'+
					'<button type="submit" class="btn btn-info btn-fill pull-right" onclick="eliminarCita(event,'+citas[cita].id_cita+','+citas[cita].id_mascota+')">Eliminar</button>'+
					'<div class="clearfix"></div>'+
					'<hr style="margin-top:10px;">'+

					'<div class="modal fade" id="citaModal'+citas[cita].id_cita+'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
						'<div class="modal-dialog" role="document">'+
							'<div class="modal-content">'+
							'<div class="modal-header">'+
									'<h5 class="modal-title" id="exampleModalLabel">Cita</h5>'+
									'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
										'<span aria-hidden="true">&times;</span>'+
									'</button>'+
								'</div>'+
								'<form id="editarInfoMascota">'+
								'<div class="modal-body">'+

									'<div class="row">'+
										'<div class="col-md-12">'+
											'<div class="form-group">'+
												'<label>Fecha:</label><br>'+
												'<input type="datetime-local" name="" value="'+citas[cita].fecha_cita+'" '+
												'id="fechaCita'+citas[cita].id_cita+'">'+
											'</div>'+
										'</div>'+
									'</div>'+
									'<div class="row">'+
										'<div class="col-md-12">'+
											'<div class="form-group">'+
												'<label>Comentario:</label><br>'+
												'<input type="text" name="" value="'+citas[cita].comentario_cita+'"'+
												'id="comentarioCita'+citas[cita].id_cita+'">'+
											'</div>'+
										'</div>'+
									'</div>'+

								'</div>'+
								'<div class="modal-footer">'+
									'<button class="btn btn-primary" onclick="editarCitaMascota(event, '+citas[cita].id_cita+', '+citas[cita].id_mascota+')">Guardar</button>'+
									'<button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>'+
								'</div>'+
								'</form>'+
							'</div>'+
						'</div>'+
					'</div>';

				}

					document.getElementById('citasDespliegue').innerHTML=txt;


								}
				}
		}
	}else{
		mensaje('Por favor, completa los campos :)');
	}


}

function registrarTratamiento(e){
	e.preventDefault();

	var select=document.getElementById("selectMascotas");
	var id = select.options[select.selectedIndex].value;
	var tratamiento=document.getElementById('nombreTratamiento').value;
	var fechaTratamiento=document.getElementById('fechaTratamiento').value;
	var idForm = document.querySelector("form").id;


	if (id != "" && tratamiento != "" && fechaTratamiento!="") {
		loginAJAX = new XMLHttpRequest();
		loginAJAX.open('GET','ajax/peticiones.php?idMT='+id+'&t='+tratamiento+'&f='+fechaTratamiento);
		loginAJAX.send();

		loginAJAX.onreadystatechange = function(){
			if (loginAJAX.readyState == 4 && loginAJAX.status == 200) {
				tratamiento = JSON.parse(loginAJAX.responseText);

				if(tratamiento.status=="error"){
					mensaje('No se pudo registrar el tratamiento en la base de datos, intente de nuevo.');
				}else if(tratamiento.status=="exito"){

						mensaje('El tratamiento se registró con exito en el sistema :)')
						limpiarInputsForm(idForm);
									}
								}
				}
	}else{
		mensaje('Completa los campos por favor :)');
	}

}

function registrarCita(e){
	e.preventDefault();

	var select=document.getElementById("selectMascotas");
	var id = select.options[select.selectedIndex].value;
	var fechaCita=document.getElementById('fechaCita').value;
	var comentarioCita=document.getElementById('comentarioCita').value;
	var idForm = document.querySelector("form").id;


	if (id != "" && fechaCita != "") {
		loginAJAX = new XMLHttpRequest();
		loginAJAX.open('GET','ajax/peticiones.php?idMC='+id+'&fc='+fechaCita+'&cc='+comentarioCita);
		loginAJAX.send();

			loginAJAX.onreadystatechange = function(){
					if (loginAJAX.readyState == 4 && loginAJAX.status == 200) {
						cita = JSON.parse(loginAJAX.responseText);

								if(cita.status=="error"){

									setTimeout(function () {
										window.scrollTo({
											top:0,
											behavior: "smooth"
										});
									}, 100);

									mensaje('No se pudo registrar la cita en la base de datos, intente de nuevo.');
								}else if(cita.status=="exito"){

									setTimeout(function () {
										window.scrollTo({
											top:0,
											behavior: "smooth"
										});
									}, 100);

										mensaje('La cita se registró con éxito en el sistema :)')
										limpiarInputsForm(idForm);
								}
						}
				}
	}else{

		setTimeout(function () {
			window.scrollTo({
				top:0,
				behavior: "smooth"
			});
		}, 100);
		mensaje('Completa los campos por favor :)');
	}
}

function eliminarMascota(e, idMascota){
	e.preventDefault();

	var idMascota=idMascota;


		if (idMascota != "") {
			loginAJAX = new XMLHttpRequest();
			loginAJAX.open('GET','ajax/peticiones.php?idMELI='+idMascota);
			loginAJAX.send();

				loginAJAX.onreadystatechange = function(){
						if (loginAJAX.readyState == 4 && loginAJAX.status == 200) {
							eliminacionMascota = JSON.parse(loginAJAX.responseText);

									if(eliminacionMascota.status=="error"){
										setTimeout(function () {
											window.scrollTo({
												top:0,
												behavior: "smooth"
											});
										}, 100);
										mensaje('No se pudo eliminar la mascota, intente de nuevo.');
									}else if(eliminacionMascota.status=="exito"){
										setTimeout(function () {
											window.scrollTo({
												top:0,
												behavior: "smooth"
											});
										}, 100);
											mensaje('La mascota se eliminó con éxito');
											setTimeout(function () {
												window.location.href = "dashboard.php";
										 }, 1500);

									}
							}
					}
		}else{
			mensaje('Completa los campos por favor :)');
		}



}

function eliminarCita(e, idCita, idMascota){
	e.preventDefault();

	var modal =document.getElementById("citaModal"+idCita);
	var idCita=idCita;
	var idMascota=idMascota;

	var txt="";

	loginAJAX = new XMLHttpRequest();
	loginAJAX.open('GET','ajax/peticiones.php?idMCELI='+idMascota+'&idceli='+idCita);
	loginAJAX.send();
	loginAJAX.onreadystatechange = function(){

		if (loginAJAX.readyState == 4 && loginAJAX.status == 200) {

			citas = JSON.parse(loginAJAX.responseText);

			if(citas.status=="error"){
						$(modal).modal('hide');

						setTimeout(function () {
							window.scrollTo({
								top:0,
								behavior: "smooth"
							});
						}, 500);

						mensaje('No se pudo eliminar la cita. Intente de nuevo');

			}else{

				$(modal).modal('hide');

				setTimeout(function () {
					window.scrollTo({
						top:0,
						behavior: "smooth"
					});
				}, 500);

				mensaje ("Se eliminó exitosamente la cita en la base de datos");

				for (cita in citas){
					txt+='<div class="row">'+
					'<div class="col-md-6">'+
					'<div class="form-group">'+
							'<label>Fecha:</label><br>'+
							'<label>'+citas[cita].fecha_cita+'</label>'+
						'</div>'+
					'</div>'+
					'<div class="col-md-6">'+
						'<div class="form-group">'+
							'<label>Comentario:</label><br><label>'+
							citas[cita].comentario_cita+
							'</label>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<button type="button" class="btn btn-info btn-fill pull-right" data-toggle="modal" data-target="#citaModal'+citas[cita].id_cita+'">'+
					'Editar'+
				'</button>'+
				'<button type="submit" class="btn btn-info btn-fill pull-right" onclick="eliminarCita(event, '+
					citas[cita].id_cita+','+citas[cita].id_mascota+')">Eliminar</button>'+
				'<div class="clearfix"></div>'+
				'<hr style="margin-top:10px;">'+

				'<div class="modal fade" id="citaModal'+citas[cita].id_cita+'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
					'<div class="modal-dialog" role="document">'+
						'<div class="modal-content">'+
						'<div class="modal-header">'+
								'<h5 class="modal-title" id="exampleModalLabel">Cita</h5>'+
								'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
									'<span aria-hidden="true">&times;</span>'+
								'</button>'+
							'</div>'+
							'<form id="editarInfoMascota">'+
							'<div class="modal-body">'+

								'<div class="row">'+
									'<div class="col-md-12">'+
										'<div class="form-group">'+
											'<label>Fecha:</label><br>'+
											'<input type="datetime-local" name="" value="'+citas[cita].fecha_cita+'" '+
											'id="fechaCita'+citas[cita].id_cita+'">'+
										'</div>'+
									'</div>'+
								'</div>'+
								'<div class="row">'+
									'<div class="col-md-12">'+
										'<div class="form-group">'+
											'<label>Comentario:</label><br>'+
											'<input type="text" name="" value="'+citas[cita].comentario_cita+'"'+
											'id="comentarioCita'+citas[cita].id_cita+'">'+
										'</div>'+
									'</div>'+
								'</div>'+

							'</div>'+
							'<div class="modal-footer">'+
								'<button class="btn btn-primary" onclick="editarCitaMascota(event, '+citas[cita].id_cita+', '+citas[cita].id_mascota+')">Guardar</button>'+
								'<button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>'+
							'</div>'+
							'</form>'+
						'</div>'+
					'</div>'+
				'</div>';

			}

				document.getElementById('citasDespliegue').innerHTML=txt;


							}
			}
	}

}

function irArriba(e, modal){
	e.preventDefault();

	$("tratamientoModal"+modal).scrollTop(0);
	// window.scrollTo({
	// 	top:0,
	// 	behavior: "smooth"
	// });
}

function mostrarMensaje(e){
	e.preventDefault();
	mensaje('prueba');
}

function limpiarInputsForm(idForm){
	var inputs = document.getElementById(''+idForm).querySelectorAll('input');
	for (var i = 0; i < inputs.length; i++) {
		inputs[i].value = "";
	}
}


function addBiblioteca(e){
	e.preventDefault();
	fondo=document.querySelector('.fondo-modal');
	modal=document.querySelector('.modal-personalizado');
	fondo.style.display="block";
	fondo.style.transition=".3s all";
	setTimeout(function(){
		fondo.style.opacity="1"
	},100);
	modal.style.display="block";
	modal.style.transition=".3s all";
	setTimeout(function(){
		modal.style.opacity="1"
	},100);


}


function cerrarAddBiblioteca(e){
	e.preventDefault();
	fondo=document.querySelector('.fondo-modal');
	modal=document.querySelector('.modal-personalizado');
	fondo.style.opacity="0"
	fondo.style.transition=".3 all";
	setTimeout(function(){
		fondo.style.display="none";
	},300);
	modal.style.opacity="0"
	modal.style.transition=".3 all";
	setTimeout(function(){
		modal.style.display="none";
	},300);
	// document.querySelector(".modal-personalizado").querySelectorAll('input').value="";
	// document.getElementById('nombre-biblioteca').value="";
	var idForm = document.querySelector("form").id;
	limpiarInputsForm(idForm);
}
