<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include 'vistas-base/head.php';?>
  </head>
  <body onload="imprimirSesion()">
    <?php include 'php/verificarLogin.php';?>
    <nav>
      <?php include 'vistas-base/nav.php';?>
  	</nav>

    <aside>
      <?php include 'vistas-base/sidenav.php';?>
  </aside>
<main>

  <div class="dashboard-bienvenida">
    <h1>Bienvenido a HappyPets</h1>
    <img src="resources/images/dog2.jpg" alt="logoPerrito" id="logo-perrito" class="center">
  </div>

</main>


  </body>
</html>
